package xyz.romakononovich.instasave.domain.service

interface Clipboard {
    fun download(url: String)
    fun addDownloadTask(url: String, name: String)
}