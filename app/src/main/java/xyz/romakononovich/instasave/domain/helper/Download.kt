package xyz.romakononovich.instasave.domain.helper

import io.reactivex.Observable
import xyz.romakononovich.instasave.data.model.download.Response

interface Download {
    fun getResponseObject(clipboardUrl: String): Observable<Response>
}