package xyz.romakononovich.instasave.domain.helper

import android.content.Context

interface Notification {
    fun show(context: Context)
    fun hide(context: Context)
}