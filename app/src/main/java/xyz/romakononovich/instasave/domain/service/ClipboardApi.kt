package xyz.romakononovich.instasave.domain.service

interface ClipboardApi {
    var serviceRunning: Boolean
    fun handleService()
    fun isClipboardServiceRunning(serviceClass: Class<*>): Boolean
    fun stop()
    fun start()
}