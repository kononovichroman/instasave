package xyz.romakononovich.instasave.presentation.base

/**
 * Created by RomanK on 08.06.18.
 */
interface BasePresenter<T> {

    fun start()

    var view: T
}
