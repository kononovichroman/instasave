package xyz.romakononovich.instasave.presentation.router

import android.content.Context

/**
 * Created by RomanK on 09.06.18.
 */
interface Router {
    fun launchInstagram()
    fun openGallery()
    fun sharePhoto(path: String)
    fun playVideo(path: String)
}