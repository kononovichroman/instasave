package xyz.romakononovich.instasave.presentation.main

import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import xyz.romakononovich.instasave.R
import xyz.romakononovich.instasave.presentation.main.fragment.DownloadFragment
import xyz.romakononovich.instasave.presentation.main.fragment.ZoomAvatarFragment


/**
 * Created by Roman K. on 24.11.2018.
 * Copyright (c) 2018 ROMAN KONONOVICH. All rights reserved.
 */

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        return when (p0.itemId) {
            R.id.nav_download -> {
                fm.beginTransaction().hide(activeFragment).show(downloadFragment).commit()
                activeFragment = downloadFragment
                true

            }
            R.id.nav_zoom_avatar -> {
                fm.beginTransaction().hide(activeFragment).show(zoomAvatarFragment).commit()
                activeFragment = zoomAvatarFragment
                true
            }
            else -> false
        }
    }

    private val downloadFragment = DownloadFragment()
    private val zoomAvatarFragment = ZoomAvatarFragment()
    private lateinit var activeFragment: Fragment
    private val fm = supportFragmentManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activeFragment = downloadFragment
        fm.beginTransaction().add(R.id.mainContainer, downloadFragment, "downloadFragment").commit()
        fm.beginTransaction().add(R.id.mainContainer, zoomAvatarFragment, "zoomAvatarFragment").hide(zoomAvatarFragment).commit()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            bottomNavigationViewMorph.setOnNavigationItemSelectedListener(this)
        } else {
            bottomNavigationView.setOnNavigationItemSelectedListener(this)
        }


//        bottomNavigationView.setOnNavigationItemSelectedListener
    }
}