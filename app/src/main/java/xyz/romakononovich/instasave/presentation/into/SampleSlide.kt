package xyz.romakononovich.instasave.presentation.into

import androidx.fragment.app.Fragment
import android.os.Bundle
import androidx.annotation.Nullable
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View


/**
 * Created by RomanK on 11.06.18.
 */
class SampleSlide : androidx.fragment.app.Fragment() {

    private val ARG_LAYOUT_RES_ID = "layoutResId"
    private var layoutResId: Int = 0

    fun newInstance(layoutResId: Int): SampleSlide {
        val sampleSlide = SampleSlide()

        val args = Bundle()
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId)
        sampleSlide.arguments = args

        return sampleSlide
    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments!!.containsKey(ARG_LAYOUT_RES_ID))
            layoutResId = arguments!!.getInt(ARG_LAYOUT_RES_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResId, container, false)
    }
}