package xyz.romakononovich.instasave.presentation.router

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.widget.Toast
import xyz.romakononovich.instasave.R
import xyz.romakononovich.instasave.utils.PACKAGE_INSTAGRAM
import xyz.romakononovich.instasave.utils.DOWNLOAD_DIR
import java.io.File
import android.os.StrictMode
import xyz.romakononovich.instasave.presentation.gallery.GalleryActivity
import xyz.romakononovich.instasave.utils.IMAGE_TYPE
import androidx.core.content.ContextCompat.startActivity



/**
 * Created by RomanK on 09.06.18.
 */
class RouterImpl(private val context: Context) : Router {
    override fun launchInstagram() {
        val intent = context.packageManager.getLaunchIntentForPackage(PACKAGE_INSTAGRAM)
        try {
            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(context, context.getString(R.string.toast_no_instagram_app), Toast.LENGTH_SHORT).show()
        } catch (ex: NullPointerException) {
            Toast.makeText(context, context.getString(R.string.toast_no_instagram_app), Toast.LENGTH_SHORT).show()
        }
    }

//    override fun openGallery() {
//        val builder = StrictMode.VmPolicy.Builder()
//        StrictMode.setVmPolicy(builder.build())
//        if (File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DOWNLOAD_DIR).listFiles() != null &&
//                File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DOWNLOAD_DIR).listFiles().isNotEmpty()) {
//            val intent = Intent()
//            intent.action = Intent.ACTION_VIEW
//            intent.setDataAndType(Uri.fromFile(File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DOWNLOAD_DIR).listFiles().last()), IMAGE_TYPE)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            context.startActivity(intent)
//        } else {
//            Toast.makeText(context, context.getString(R.string.toast_album_no_found), Toast.LENGTH_SHORT).show()
//        }


    override fun openGallery() {
        val intent = Intent(context, GalleryActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }

    override fun sharePhoto(path: String) {
        val builder = StrictMode.VmPolicy.Builder() //https://stackoverflow.com/questions/48117511/exposed-beyond-app-through-clipdata-item-geturi
        StrictMode.setVmPolicy(builder.build())
        val uriToImage = Uri.fromFile(File(path))
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = IMAGE_TYPE
        shareIntent.putExtra(Intent.EXTRA_STREAM, uriToImage)

        context.startActivity(Intent.createChooser(shareIntent, context.getString(R.string.share_photo_text)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
    }

    override fun playVideo(path: String) {
        val builder = StrictMode.VmPolicy.Builder() //https://stackoverflow.com/questions/48117511/exposed-beyond-app-through-clipdata-item-geturi
        StrictMode.setVmPolicy(builder.build())
        val intent = Intent(Intent.ACTION_VIEW, Uri.fromFile(File(path)))
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.setDataAndType(Uri.fromFile(File(path)), "video/mp4")
        context.startActivity(intent)
    }


}