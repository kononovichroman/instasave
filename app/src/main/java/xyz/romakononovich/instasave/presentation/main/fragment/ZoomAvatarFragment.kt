package xyz.romakononovich.instasave.presentation.main.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.fragment_zoom_avatar.*
import org.koin.android.ext.android.inject
import xyz.romakononovich.instasave.BuildConfig
import xyz.romakononovich.instasave.R
import xyz.romakononovich.instasave.data.model.zoom.HdProfilePicUrlInfo
import xyz.romakononovich.instasave.presentation.main.ZoomAvatarContract
import xyz.romakononovich.instasave.utils.AD_UNIT_ID_DEBUG
import xyz.romakononovich.instasave.utils.AD_UNIT_ID_RELEASE
import xyz.romakononovich.instasave.utils.hideKeyboard


/**
 * Created by Roman K. on 24.11.2018.
 * Copyright (c) 2018 ROMAN KONONOVICH. All rights reserved.
 */

class ZoomAvatarFragment : Fragment(), ZoomAvatarContract.View {

    override val presenter by inject<ZoomAvatarContract.Presenter>()
    private lateinit var interstitialAd: InterstitialAd

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_zoom_avatar, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.view = this
        interstitialAd = InterstitialAd(activity)
        interstitialAd.adUnitId = if (BuildConfig.DEBUG) {
            AD_UNIT_ID_DEBUG
        } else {
            AD_UNIT_ID_RELEASE
        }
        setListeners()

    }

    private fun setListeners() {
        fragmentZoomAvatarBtnSearch.setOnClickListener {
            clickSearch()
        }
        fragmentZoomAvatarEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                clickSearch()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun clickSearch() {
        fragmentZoomAvatarDescription.visibility = View.GONE
        fragmentZoomAvatarEditText.hideKeyboard()
        fragmentZoomAvatarBtnSearchIvPhoto.setImageResource(android.R.color.transparent)
        showLoading()
        presenter.searchByUsername(fragmentZoomAvatarEditText.text.toString())
        interstitialAd.show()
    }

    override fun errorLoadAvatar() {
        fragmentZoomAvatarIvError.visibility = View.VISIBLE
        fragmentZoomAvatarTvError.visibility = View.VISIBLE
        hideLoading()
    }

    override fun showAvatar(hdProfilePicUrlInfo: HdProfilePicUrlInfo, username: String?) {
        Glide.with(fragmentZoomAvatarBtnSearchIvPhoto)
                .load(hdProfilePicUrlInfo.url)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        hideLoading()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        hideLoading()
                        return false
                    }

                })
                .into(fragmentZoomAvatarBtnSearchIvPhoto)

        fragmentZoomAvatarUserName.apply {
            text = username
            visibility = View.VISIBLE
        }

    }

    override fun showLoading() {
        fragmentZoomAvatarProgressBar.visibility = View.VISIBLE
        fragmentZoomAvatarIvError.visibility = View.GONE
        fragmentZoomAvatarTvError.visibility = View.GONE
        fragmentZoomAvatarUserName.visibility = View.GONE
    }

    override fun hideLoading() {
        fragmentZoomAvatarProgressBar.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        interstitialAd.loadAd(AdRequest.Builder().build())
    }

}