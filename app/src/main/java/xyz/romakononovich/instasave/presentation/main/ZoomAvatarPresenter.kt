package xyz.romakononovich.instasave.presentation.main

import xyz.romakononovich.instasave.data.helper.ZoomAvatarHelper
import xyz.romakononovich.instasave.data.model.zoom.HdProfilePicUrlInfo
import xyz.romakononovich.instasave.presentation.router.Router


/**
 * Created by Roman K. on 24.11.2018.
 * Copyright (c) 2018 ROMAN KONONOVICH. All rights reserved.
 */

class ZoomAvatarPresenter(private val router: Router): ZoomAvatarContract.Presenter, ZoomAvatarHelper.Listener {
    override fun responseAvatar(hdProfilePicUrlInfo: HdProfilePicUrlInfo, username: String?) {
        view.showAvatar(hdProfilePicUrlInfo, username)
    }

    override fun errorAvatar() {
        view.errorLoadAvatar()
    }

    override fun searchByUsername(username: String) {
        ZoomAvatarHelper().getIdUser(username, this)

    }

    override fun saveAvatar() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun start() {
    }

    override lateinit var view: ZoomAvatarContract.View
}