package xyz.romakononovich.instasave.presentation.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import android.content.Intent
import android.preference.PreferenceManager
import xyz.romakononovich.instasave.presentation.into.IntroActivity
import xyz.romakononovich.instasave.presentation.main.MainActivity


/**
 * Created by RomanK on 11.06.18.
 */
const val FIRST_START = "isFirstStart"

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val t = Thread(Runnable {
            val getPrefs = PreferenceManager
                    .getDefaultSharedPreferences(this)

            val isFirstStart = getPrefs.getBoolean(FIRST_START, true)

            if (isFirstStart) {
                startActivity(Intent(this, IntroActivity::class.java))

                val e = getPrefs.edit()
                e.putBoolean(FIRST_START, false)
                e.apply()
            } else {
                startActivity(Intent(this, MainActivity::class.java))
            }
            finish()
        })
        t.start()
    }

}