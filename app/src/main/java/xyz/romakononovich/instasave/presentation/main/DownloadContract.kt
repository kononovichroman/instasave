package xyz.romakononovich.instasave.presentation.main

import xyz.romakononovich.instasave.presentation.base.BasePresenter
import xyz.romakononovich.instasave.presentation.base.BaseView

/**
 * Created by RomanK on 08.06.18.
 */
interface DownloadContract {

    interface View : BaseView<Presenter> {
        fun showStart()
        fun hideStart()
    }

    interface Presenter: BasePresenter<View> {
        fun startService()
        fun stopService()
        fun isRunningService(): Boolean
        fun openInstagram()
        fun openGallery()
    }
}