package xyz.romakononovich.instasave.presentation.main

import xyz.romakononovich.instasave.data.model.zoom.HdProfilePicUrlInfo
import xyz.romakononovich.instasave.presentation.base.BasePresenter
import xyz.romakononovich.instasave.presentation.base.BaseView


/**
 * Created by Roman K. on 24.11.2018.
 * Copyright (c) 2018 ROMAN KONONOVICH. All rights reserved.
 */

interface ZoomAvatarContract {
    interface View : BaseView<Presenter> {
        fun showLoading()
        fun hideLoading()
        fun showAvatar(hdProfilePicUrlInfo: HdProfilePicUrlInfo, username: String?)
        fun errorLoadAvatar()
    }

    interface Presenter : BasePresenter<View> {
        fun searchByUsername(username: String)
        fun saveAvatar()
    }
}