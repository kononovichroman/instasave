package xyz.romakononovich.instasave.presentation.main

import xyz.romakononovich.instasave.data.service.ClipboardService
import xyz.romakononovich.instasave.domain.service.ClipboardApi
import xyz.romakononovich.instasave.presentation.router.Router

/**
 * Created by RomanK on 08.06.18.
 */
class DownloadPresenter(private val serviceApi: ClipboardApi, private val router: Router) : DownloadContract.Presenter {



    override fun start() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override lateinit var view: DownloadContract.View

    override fun startService() {
        serviceApi.start()
        view.showStart()
    }

    override fun stopService() {
        serviceApi.stop()
        view.hideStart()
    }

    override fun openInstagram() {
        router.launchInstagram()
    }

    override fun openGallery() {
        router.openGallery()
    }

    override fun isRunningService(): Boolean {
        return serviceApi.isClipboardServiceRunning(ClipboardService::class.java)
    }
}