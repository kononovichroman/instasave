package xyz.romakononovich.instasave.presentation.gallery

import xyz.romakononovich.instasave.presentation.base.BasePresenter
import xyz.romakononovich.instasave.presentation.base.BaseView

interface GalleryContract {
    interface View : BaseView<Presenter> {
        fun printPhoto(path: String)

        fun initViewPager(list: MutableList<String>)

        fun refreshListPager(list: MutableList<String>)

        fun showCannotOpenGalleryToastEmpty()

        fun showCannotOpenGalleryToastPermission()
    }

    interface Presenter: BasePresenter<View> {

        fun shareFile(id: Int)

        fun printPhoto(id: Int)

        fun deleteFile(id: Int)

        fun refreshList()

        fun playVideo(path: String)

      }
}