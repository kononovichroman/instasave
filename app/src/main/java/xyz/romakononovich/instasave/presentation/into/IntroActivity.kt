package xyz.romakononovich.instasave.presentation.into

import android.os.Bundle
import com.github.paolorotolo.appintro.AppIntro
import android.content.Intent
import xyz.romakononovich.instasave.R
import xyz.romakononovich.instasave.presentation.main.MainActivity


/**
 * Created by RomanK on 11.06.18.
 */
class IntroActivity: AppIntro() {
    override fun onNextPressed() {

    }

    override fun init(savedInstanceState: Bundle?) {
        addSlide(SampleSlide().newInstance(R.layout.intro1))
        addSlide(SampleSlide().newInstance(R.layout.intro2))
        addSlide(SampleSlide().newInstance(R.layout.intro3))
        addSlide(SampleSlide().newInstance(R.layout.intro4))
        addSlide(SampleSlide().newInstance(R.layout.intro5))
    }

    override fun onDonePressed() {
        finish()
        loadMainActivity()
    }

    override fun onSlideChanged() {
    }

    override fun onSkipPressed() {
        finish()
        loadMainActivity()
    }

    private fun loadMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}