package xyz.romakononovich.instasave.presentation.base

/**
 * Created by RomanK on 08.06.18.
 */
interface BaseView<out T : BasePresenter<*>> {
    val presenter: T
}
