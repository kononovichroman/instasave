package xyz.romakononovich.instasave.presentation.gallery

import android.content.Context
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_pager.view.*
import xyz.romakononovich.instasave.R
import xyz.romakononovich.instasave.utils.VIDEO_EXTENSION


class GalleryAdapter(context: Context) : androidx.viewpager.widget.PagerAdapter() {
    private lateinit var context: Context
    private lateinit var pathsList: MutableList<String>
    private var clickListener: ClickListener? = null
    private val inflater = LayoutInflater.from(context)

    constructor(context: Context, images: List<String>, listener: ClickListener) : this(context) {
        this.context = context
        this.pathsList = images.toMutableList()
        clickListener = listener
    }

    fun delete(id: Int) {
        pathsList.removeAt(id)
        if (pathsList.size == 0) run {
            clickListener?.onLastPageDelete()
        }
        notifyDataSetChanged()
    }

    fun refresh(list: List<String>) {
        if (list != pathsList) {
            pathsList = list.toMutableList()
            notifyDataSetChanged()
        }
    }

    interface ClickListener {
        fun onClickViewPager()
        fun onClickPlay(path: String)
        fun onLastPageDelete()
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return pathsList.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return androidx.viewpager.widget.PagerAdapter.POSITION_NONE
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = inflater.inflate(R.layout.item_pager, container, false)
        Glide.with(context)
                .load(pathsList[position])
                .into(itemView.ivPhoto)
        if (pathsList[position].contains(VIDEO_EXTENSION)) {
            itemView.ivPhoto.isZoomable = false
            itemView.btnPlay.visibility = View.VISIBLE
        } else {
            itemView.ivPhoto.isZoomable = true
            itemView.btnPlay.visibility = View.INVISIBLE
        }
        itemView.btnPlay.setOnClickListener {
            clickListener?.onClickPlay(pathsList[position])

        }
        itemView.setOnClickListener {
            clickListener?.onClickViewPager()
        }
        container.addView(itemView)

        return itemView

    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}