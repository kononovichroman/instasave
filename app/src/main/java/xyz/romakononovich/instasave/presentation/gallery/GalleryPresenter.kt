package xyz.romakononovich.instasave.presentation.gallery

import xyz.romakononovich.instasave.presentation.router.Router
import xyz.romakononovich.instasave.utils.deleteFile
import xyz.romakononovich.instasave.utils.getSortedByNameListFiles

class GalleryPresenter(private val router: Router): GalleryContract.Presenter {


    override lateinit var view: GalleryContract.View

    override fun shareFile(id: Int) {
        router.sharePhoto(getSortedByNameListFiles()[id])
    }

    override fun printPhoto(id: Int) {
        view.printPhoto(getSortedByNameListFiles()[id])
    }

    override fun deleteFile(id: Int) {
        deleteFile(getSortedByNameListFiles()[id])
    }

    override fun refreshList() {
        view.refreshListPager(getSortedByNameListFiles())
    }

    override fun start() {
        view.initViewPager(getSortedByNameListFiles())
    }

    override fun playVideo(path: String) {
        router.playVideo(path)
    }

}