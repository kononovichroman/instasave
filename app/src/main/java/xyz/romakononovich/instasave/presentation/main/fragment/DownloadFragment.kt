package xyz.romakononovich.instasave.presentation.main.fragment

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import androidx.browser.customtabs.CustomTabsIntent
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.fragment_download.*
import org.koin.android.ext.android.inject
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions
import xyz.romakononovich.instasave.BuildConfig
import xyz.romakononovich.instasave.R
import xyz.romakononovich.instasave.presentation.into.IntroActivity
import xyz.romakononovich.instasave.presentation.main.DownloadContract
import xyz.romakononovich.instasave.utils.AD_UNIT_ID_DEBUG
import xyz.romakononovich.instasave.utils.AD_UNIT_ID_RELEASE
import xyz.romakononovich.instasave.utils.URL_PRIVACY_POLICY
import xyz.romakononovich.instasave.utils.toast


@RuntimePermissions
class DownloadFragment : Fragment(), DownloadContract.View, CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    override val presenter by inject<DownloadContract.Presenter>()
    private lateinit var interstitialAd: InterstitialAd


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_download, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.view = this
        interstitialAd = InterstitialAd(activity)
        interstitialAd.adUnitId = if (BuildConfig.DEBUG) {
            AD_UNIT_ID_DEBUG
        } else {
            AD_UNIT_ID_RELEASE
        }
        switchService.setOnCheckedChangeListener(this)
        tvOpenInstagram.setOnClickListener(this)
        tvOpenGallery.setOnClickListener(this)
        btnHelp.setOnClickListener(this)
        tvPrivacyPolicy.setOnClickListener(this)
    }
    override fun showStart() {
        wave.start()
        iv.setImageResource(R.drawable.ic_instagram)
    }

    override fun hideStart() {
        wave.pause()
        iv.setImageResource(R.drawable.ic_instagram_bw)
    }

    override fun onStart() {
        super.onStart()
        if (presenter.isRunningService()) {
            switchService.isChecked = true
            showStart()
        }
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun startService() {
        presenter.startService()
        tvOpenInstagram.isEnabled = true
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun openGallery() {
        presenter.openGallery()
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showToastPermission() {
        hideStart()
        switchService.isChecked = false
        activity?.toast(getString(R.string.toast_need_permission))
    }

    private fun stopService() {
        interstitialAd.show()
        presenter.stopService()
    }

    override fun onResume() {
        super.onResume()
        interstitialAd.loadAd(AdRequest.Builder().build())
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        DownloadFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
        if (isChecked) {
            if (!presenter.isRunningService()) {
                DownloadFragmentPermissionsDispatcher.startServiceWithCheck(this)
            } else {
                tvOpenInstagram.isEnabled = true
            }
        } else {
            stopService()
            tvOpenInstagram.isEnabled = false
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            tvOpenInstagram -> presenter.openInstagram()
            tvOpenGallery -> DownloadFragmentPermissionsDispatcher.openGalleryWithCheck(this)
            btnHelp -> startActivity(Intent(activity, IntroActivity::class.java))
            tvPrivacyPolicy -> CustomTabsIntent.Builder().build().launchUrl(activity, Uri.parse(URL_PRIVACY_POLICY))
        }
    }
}
