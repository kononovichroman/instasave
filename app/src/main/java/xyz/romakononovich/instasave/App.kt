package xyz.romakononovich.instasave

import androidx.multidex.MultiDexApplication
import org.koin.android.ext.android.startKoin
import xyz.romakononovich.instasave.di.koinServiceApi
import com.crashlytics.android.Crashlytics
import com.google.android.gms.ads.MobileAds
import io.fabric.sdk.android.Fabric


class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(koinServiceApi))
        Fabric.with(this, Crashlytics())
        MobileAds.initialize(this, "ca-app-pub-4197186830381954~9424933268")
    }
}