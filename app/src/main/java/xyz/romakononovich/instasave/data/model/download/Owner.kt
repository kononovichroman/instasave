package xyz.romakononovich.instasave.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by RomanK on 10.06.18.
 */
data class Owner(

        @SerializedName("id")
        @Expose
        var id: String? = null,
        @SerializedName("profile_pic_url")
        @Expose
        var profilePicUrl: String? = null,
        @SerializedName("username")
        @Expose
        var username: String? = null

)
