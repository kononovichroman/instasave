package xyz.romakononovich.instasave.data.helper

import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import xyz.romakononovich.instasave.data.model.download.Response
import xyz.romakononovich.instasave.data.model.searchid.SearchUsers
import xyz.romakononovich.instasave.data.model.zoom.ResponseUser

/**
 * Created by RomanK on 10.06.18.
 */
interface NetworkInterface {
    @GET("?__a=1")
    fun getDownloadResponse(): Observable<Response>


    @GET("web/search/topsearch/")
    fun getIdUser(@Query("query") username: String): Observable<SearchUsers>

    @GET("{id}/info/")
    fun getUser(@Path("id") idUser: String): Observable<ResponseUser>

 }