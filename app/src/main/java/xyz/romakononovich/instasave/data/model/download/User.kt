package xyz.romakononovich.instasave.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by RomanK on 10.06.18.
 */
data class User(

        @SerializedName("username")
        @Expose
        var username: String? = null

)