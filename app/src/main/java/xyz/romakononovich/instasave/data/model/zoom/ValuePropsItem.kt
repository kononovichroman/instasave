package xyz.romakononovich.instasave.data.model.zoom

import com.google.gson.annotations.SerializedName

data class ValuePropsItem(

        @SerializedName("name")
        val name: String? = null,

        @SerializedName("description")
        val description: String? = null,

        @SerializedName("type")
        val type: Int? = null
)