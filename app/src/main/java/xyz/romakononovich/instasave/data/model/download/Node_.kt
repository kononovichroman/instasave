package xyz.romakononovich.instasave.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by RomanK on 10.06.18.
 */
data class Node_(

        @SerializedName("__typename")
        @Expose
        var typename: String? = null,
        @SerializedName("id")
        @Expose
        var id: String? = null,
        @SerializedName("shortcode")
        @Expose
        var shortcode: String? = null,
        @SerializedName("gating_info")
        @Expose
        var gatingInfo: Any? = null,
        @SerializedName("media_preview")
        @Expose
        var mediaPreview: Any? = null,
        @SerializedName("display_url")
        @Expose
        var displayUrl: String? = null,
        @SerializedName("video_url")
        @Expose
        var videoUrl: String? = null,
        @SerializedName("video_view_count")
        @Expose
        var videoViewCount: Int? = null,
        @SerializedName("is_video")
        @Expose
        var isVideo: Boolean? = null,
        @SerializedName("should_log_client_event")
        @Expose
        var shouldLogClientEvent: Boolean? = null,
        @SerializedName("tracking_token")
        @Expose
        var trackingToken: String? = null

)