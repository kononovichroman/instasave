package xyz.romakononovich.instasave.data.helper

import android.annotation.SuppressLint
import android.util.Log
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import xyz.romakononovich.instasave.data.model.searchid.SearchUsers
import xyz.romakononovich.instasave.data.model.zoom.HdProfilePicUrlInfo
import xyz.romakononovich.instasave.data.model.zoom.ResponseUser


/**
 * Created by Roman K. on 24.11.2018.
 * Copyright (c) 2018 ROMAN KONONOVICH. All rights reserved.
 */
@SuppressLint("CheckResult")

class ZoomAvatarHelper {
//

    fun getIdUser(username: String, listener: Listener) {

        getResponseByUsername(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    if (response.users?.isNotEmpty() == true) {
                        response.users[0]?.user?.pk?.let { getUser(it, response.users[0]?.user?.fullName, listener) }
                    } else {
                        error("No found user")
                    }
                }, {
                    Log.d(this.javaClass.name, "Throwable: ${it.localizedMessage}")
                    listener.errorAvatar()
                })

    }

    private fun getUser(id: String, username: String?, listener: Listener) {
        getResponseUser(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ responseUser ->
                    responseUser.user?.hdProfilePicUrlInfo?.let { listener.responseAvatar(it, username) }
                }, {
                    listener.errorAvatar()
                })
    }

    interface Listener {
        fun responseAvatar(hdProfilePicUrlInfo: HdProfilePicUrlInfo, username: String?)
        fun errorAvatar()
    }

    private fun getResponseByUsername(str: String): Observable<SearchUsers> {
        val username: String? = try {
            if (str.contains("instagram.com")) {
                val substringPosition = str.indexOf("instagram.com/") + 14
                var tempStr = str.substring(substringPosition)
                if (tempStr.contains("/")){
                    tempStr = tempStr.substring(0, tempStr.indexOf("/", 0, true))
                }
                if (tempStr.contains("?")){
                    tempStr = tempStr.substring(0, tempStr.indexOf("?", 0, true))
                }
                tempStr
            } else {
                str
            }
        } catch (e: StringIndexOutOfBoundsException) {
            null
        }

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://www.instagram.com/").build()
                .create(NetworkInterface::class.java).getIdUser(username ?: "")
    }

    private fun getResponseUser(id: String): Observable<ResponseUser> {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://i.instagram.com/api/v1/users/").build()
                .create(NetworkInterface::class.java).getUser(id)
    }

}