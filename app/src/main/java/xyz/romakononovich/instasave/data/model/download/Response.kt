package xyz.romakononovich.instasave.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by RomanK on 10.06.18.
 */

data class Response(

        @SerializedName("graphql")
        @Expose
        var graphql: Graphql? = null

)