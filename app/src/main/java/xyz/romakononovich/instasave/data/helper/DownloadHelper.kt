package xyz.romakononovich.instasave.data.helper

import com.google.gson.GsonBuilder
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import xyz.romakononovich.instasave.data.model.download.Response
import xyz.romakononovich.instasave.domain.helper.Download

class DownloadHelper : Download {
    override fun getResponseObject(clipboardUrl: String): Observable<Response> {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(clipboardUrl).build()
                .create(NetworkInterface::class.java).getDownloadResponse()
    }

}