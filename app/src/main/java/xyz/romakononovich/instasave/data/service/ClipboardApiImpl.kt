package xyz.romakononovich.instasave.data.service

import android.content.Context
import xyz.romakononovich.instasave.domain.service.ClipboardApi
import android.app.ActivityManager
import android.content.Intent
import android.os.Build


class ClipboardApiImpl(private val context: Context): ClipboardApi {
    override fun start() {
        serviceRunning = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(Intent(context,ClipboardService::class.java))
        }
        context.startService(Intent(context,ClipboardService::class.java))
    }

    override fun stop() {
        context.stopService(Intent(context,ClipboardService::class.java))
    }

    override var serviceRunning = false

    override fun handleService() {
        if (serviceRunning) {
            start()
            serviceRunning = false
        } else {
            start()
        }
    }

    override fun isClipboardServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}