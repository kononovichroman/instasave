package xyz.romakononovich.instasave.data.model.zoom

import com.google.gson.annotations.SerializedName

data class ResponseUser(
	@SerializedName("user")
	val user: User? = null,

	@SerializedName("status")
	val status: String? = null
)