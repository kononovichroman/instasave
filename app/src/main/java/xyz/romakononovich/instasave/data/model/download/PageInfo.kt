package xyz.romakononovich.instasave.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by RomanK on 10.06.18.
 */
data class PageInfo(

        @SerializedName("has_next_page")
        @Expose
        var hasNextPage: Boolean? = null,
        @SerializedName("end_cursor")
        @Expose
        var endCursor: Any? = null

)