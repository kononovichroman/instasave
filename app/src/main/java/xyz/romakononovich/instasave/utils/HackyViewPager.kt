package xyz.romakononovich.instasave.utils

import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * Created by RomanK on 17.06.18.
 */
class HackyViewPager(context: Context, attributeSet: AttributeSet) :
        androidx.viewpager.widget.ViewPager(context, attributeSet) {
    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return try {
            super.onInterceptTouchEvent(ev)
        } catch (e: IllegalArgumentException) {
            false
        }
    }
}