package xyz.romakononovich.instasave.utils

import android.content.Context
import android.os.Build
import android.os.Environment
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import java.io.File

fun Context.toast(message: CharSequence): Toast = Toast
        .makeText(this, message, Toast.LENGTH_SHORT)
        .apply {
            show()
        }

fun getSortedByNameListFiles(): MutableList<String> {
    val pathsList: MutableList<String> = ArrayList()
    return if (getListFiles() != null) {
        for (image in getListFiles().sortedWith(Comparator<File> { p0, p1 -> p0.name.compareTo(p1.name) }).reversed()) {
            pathsList.add(image.absolutePath)
        }
        pathsList
    } else {
        pathsList
    }
}

fun getListFiles() = File(Environment.getExternalStoragePublicDirectory(
        Environment.DIRECTORY_PICTURES), DOWNLOAD_DIR).listFiles()

fun deleteFile(path: String) {
    File(path).delete()
}

inline fun ifLollipop(action1: () -> Unit, action2: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        action1()
    } else {
        action2()
    }
}

inline fun ifLollipop(action1: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        action1()
    }
}

fun checkFolder(){
    val folder = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DOWNLOAD_DIR)
    if (!folder.exists()){
        folder.mkdirs()
    }
}

fun EditText.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}