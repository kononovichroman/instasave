package xyz.romakononovich.instasave.utils

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import xyz.romakononovich.instasave.R

class DeleteDialog : androidx.fragment.app.DialogFragment() {

    companion object {
        fun newInstance(id: Int) =
                DeleteDialog().apply {
                    arguments = Bundle(1).apply {
                        putInt(IMAGE_ID, id)
                    }
                }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val id: Int = arguments?.getInt(IMAGE_ID) ?:0

        val dialogListener = activity

        return if (dialogListener is DeleteDialogListener) {
            AlertDialog.Builder(dialogListener)
                    .setMessage(R.string.dialog_message_delete)
                    .setPositiveButton(R.string.dialog_btn_delete, { _, _ ->
                        dialogListener.onDeleteDialogPositiveClick(id)
                    })
                    .setNegativeButton(R.string.dialog_dtn_cancel, { _, _ ->
                        dismiss()
                    })
                    .setCancelable(false)
                    .create()
        } else {
            super.onCreateDialog(savedInstanceState)
        }
    }

    interface DeleteDialogListener {
        fun onDeleteDialogPositiveClick(id: Int)
    }
}