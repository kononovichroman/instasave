package xyz.romakononovich.instasave.utils


/**
 * Created by Roman K. on 23.11.2018.
 * Copyright (c) 2018 ROMAN KONONOVICH. All rights reserved.
 */
 
const val AD_UNIT_ID_DEBUG = "ca-app-pub-3940256099942544/1033173712"
const val AD_UNIT_ID_RELEASE = "ca-app-pub-4197186830381954/7644947736"