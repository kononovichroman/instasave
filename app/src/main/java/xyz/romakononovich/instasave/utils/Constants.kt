package xyz.romakononovich.instasave.utils

const val URL_START = "https://www.instagram.com/p/"
const val URL_DELIMITERS = "?"
const val IMAGE_TYPE = "image/*"
const val IMAGE_EXTENSION = ".jpg"
const val VIDEO_EXTENSION = ".mp4"
const val DOWNLOAD_DIR = "Insa"
const val IMAGE_ID = "Image id"
const val DELETE_DIALOG = "Delete Dialog"
const val PACKAGE_INSTAGRAM = "com.instagram.android"
const val URL_PRIVACY_POLICY = "http://romakononovich.xyz/privacy.html"