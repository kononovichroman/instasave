package xyz.romakononovich.instasave.di

import org.koin.dsl.module.applicationContext
import xyz.romakononovich.instasave.data.service.ClipboardApiImpl
import xyz.romakononovich.instasave.domain.service.ClipboardApi
import xyz.romakononovich.instasave.presentation.gallery.GalleryContract
import xyz.romakononovich.instasave.presentation.gallery.GalleryPresenter
import xyz.romakononovich.instasave.presentation.main.DownloadContract
import xyz.romakononovich.instasave.presentation.main.DownloadPresenter
import xyz.romakononovich.instasave.presentation.main.ZoomAvatarContract
import xyz.romakononovich.instasave.presentation.main.ZoomAvatarPresenter
import xyz.romakononovich.instasave.presentation.router.Router
import xyz.romakononovich.instasave.presentation.router.RouterImpl

val koinServiceApi = applicationContext {
    factory { DownloadPresenter(get(), get()) as DownloadContract.Presenter }
    factory { ZoomAvatarPresenter(get()) as ZoomAvatarContract.Presenter }
    factory { GalleryPresenter(get()) as GalleryContract.Presenter }
    bean { ClipboardApiImpl(get()) as ClipboardApi }
    bean { RouterImpl(get()) as Router }
}