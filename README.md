# Insa - Save photo and video from Instаgram

[![Build status](https://ci.appveyor.com/api/projects/status/alfxv1yd9a4jpxry/branch/master?svg=true)](https://ci.appveyor.com/project/kononovichroman/instasave/branch/master)
[![Min API Level](https://img.shields.io/badge/min%20API-16-brightgreen.svg)](#)
[![Version](https://img.shields.io/badge/version-1.0.3%20release-brightgreen.svg)](#)

[![Icon](http://romakononovich.xyz/git/insa/logo.png)](#)
[![Icon](http://romakononovich.xyz/git/insa/google-play-badge.png)](https://play.google.com/store/apps/details?id=xyz.romakononovich.instasave)

## Screenshots

[![Screen1](http://romakononovich.xyz/git/insa/screen1.jpg)](#)
[![Screen1](http://romakononovich.xyz/git/insa/screen2.jpg)](#)
[![Screen1](http://romakononovich.xyz/git/insa/screen3.jpg)](#)
[![Screen1](http://romakononovich.xyz/git/insa/screen4.jpg)](#)
[![Screen1](http://romakononovich.xyz/git/insa/screen5.jpg)](#)

License
--------

    Copyright 2018 Kononovich Roman

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
